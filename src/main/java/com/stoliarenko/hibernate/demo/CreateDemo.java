package com.stoliarenko.hibernate.demo;

import com.stoliarenko.hibernate.demo.entity.Instructor;
import com.stoliarenko.hibernate.demo.entity.InstructorDetail;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;



public class CreateDemo {

    public static void main(String[] args) {
        //create session factory
        SessionFactory factory = new Configuration()
                .configure("hibernate.cfg.xml")
                .addAnnotatedClass(Instructor.class)
                .addAnnotatedClass(InstructorDetail.class)
                .buildSessionFactory();
        //create session
        Session session = factory.getCurrentSession();
        try{
            //create the object
//            Instructor temInstructor = new Instructor("Chad", "Darby", "darby@luv2code.com");
//            InstructorDetail tempInstructorDetail = new InstructorDetail("http://www.luv2code.com/youtube", "Luv 2 code!!!");

            Instructor temInstructor = new Instructor("Nikolai1", "Stoliarenko1", "kollnameshop2@gmail.com");
            InstructorDetail tempInstructorDetail = new InstructorDetail("http://nika-wedding.com1", "Luv 2 code2!!!");

            //associate the objects
            temInstructor.setInstructorDetail(tempInstructorDetail);

            //start a transaction
            session.beginTransaction();

            //save the instructor
            //Note: this will ALSO save the details object
            //because of CascadeType.ALL
            System.out.println("Saving instructor: " + temInstructor);
            session.save(temInstructor);

            //commit transaction
            session.getTransaction().commit();
            System.out.println("Done!");
        }finally {
            factory.close();
        }
    }
}
